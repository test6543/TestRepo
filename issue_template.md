#### Description - describe the bug / feature / suggestion in brief : 


#### Expected behaviour  - describe what should be the expected result : 


#### GitNex version : 
#### Gitea version : 
#### Android version : 

#### Screenshots - if any:


#### Logs - if any : 
